#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getopt
import logging
import os
import sys
import time
import serial

from gpio.GPIO import GPIO
from shell.ShellCmd import ShellCmd
from network.tools import *
from config.Configures import configures
from network.Network import Network
from network.Recv import Recv
import datetime


def main(argv):

    try:
        opts, args = getopt.getopt(argv, "hct")
    except getopt.GetoptError:
        print("main.py [-h] [-c] [-t]")

        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':

            sys.exit()

        elif opt == "-c":

            sys.exit()

        elif opt == "-t":
            sys.exit()


    serialIn = serial.Serial("/dev/ttymxc2", 57600,
        timeout=1,
        bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE)

    relayR = 15
    relayStop = 12

    network = Network()
    network.start()

    while 1 :

        data = serialIn.readline()
        if len(data) != 0 and data[0] == 0x02 :

            serialIn.readline()
            while 1:
                data = serialIn.readline()
                realData = data.decode().replace('\r\n', '')

                if len(data) <= 0 :
                    continue


                if data[0] == 0x03 :
                    break

                if len(data) == 10 or len(data) == 34:
                    if Recv.running == True:
                        network.setData(realData)

                if len(data) == 10 :

                    GPIO.setValue(relayR, 0)
                    GPIO.setValue(relayStop, 0)
                    time.sleep(4)
                    GPIO.setValue(relayR, 1)
                    GPIO.setValue(relayStop, 1)

                    logging.debug(realData)

                if len(data) == 34 :
                    # 3-8位是产品代码，17-22位是生产日期，041204， 代表2004-12-04
                    if (len(Recv.productCode) == 0 or realData[2:8] == Recv.productCode) and ((datetime.date(int("20" + realData[16:18]), int(realData[18:20]), int(realData[20:22])) -  Recv.checkDate).days > 0) :
                        pass
                    else:
                        GPIO.setValue(relayR, 0)
                        GPIO.setValue(relayStop, 0)
                        time.sleep(4)
                        GPIO.setValue(relayR, 1)
                        GPIO.setValue(relayStop, 1)

                    logging.debug(realData)


        if os.path.isfile("run.log") and os.path.getsize("run.log") > (1024*1024*200):
             ShellCmd.execute('echo "" > run.log')

    serialIn.close()


if __name__ == '__main__':
    main(sys.argv[1:])
