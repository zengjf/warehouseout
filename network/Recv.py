#/usr/bin/env python
# -*- coding: utf-8 -*-

from network.tools import *
from config.Configures import configures
import threading
from gpio.GPIO import GPIO
import logging
import re
# from datetime import *
import datetime

class Recv(threading.Thread):

    # 用于反应接收线程是否是正常，如果接收线程已经异常了，发送线程也就要退出了
    receiveThreadStatus = True
    # 是否开始校验
    running = False
    # 产品代码
    productCode = ""
    # 校验时间
    checkDate = datetime.date.today() - datetime.timedelta(days=180)

    def setConnection(self, conn):
        self.conn = conn

    def run(self):

        while True:
            try:
                buf = self.conn.recv(1024)
                bufstring = buf.decode("utf-8")

                if re.search(r"RUN", bufstring) != None:
                    Recv.running = True
                    Recv.productCode = bufstring[13:19]
                    Recv.checkDate = datetime.date(int(bufstring[19:23]), int(bufstring[23:25]), int(bufstring[25:27]))
                elif re.search(r"STOP", bufstring) != None:
                    Recv.running = False
                    pass

                time.sleep(0.01)

            except :
                logging.debug('receive thread except')
                break

        self.conn.close()

        Recv.receiveThreadStatus = False

        logging.debug('receive thread disconnection')
